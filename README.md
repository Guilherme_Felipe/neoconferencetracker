# Conference Track Management

----

## Compilando o projeto
Executar o comando na pasta raíz do projeto:

```sh
mvn clean install
```


## Executando o projeto
Executar o comando na pasta onde se encontra o arquivo .jar que foi gerado: 

```sh
java -jar ConferenceTracker-1.0.0.jar C:/path/to/file/input.txt
```

package br.com.conference.tracker.utils;

import br.com.conference.tracker.models.Conference;

public class DisplayUtils {

	public static void displayConferenceTalks(Conference conference) {
		conference.getTracks().forEach(track -> {
			System.out.println(track);
			track.getSessions().forEach(session -> {
				session.getTalks().forEach(System.out::println);
			});
			System.out.println();
		});
	}

}

package br.com.conference.tracker.models;

import java.util.ArrayList;
import java.util.List;

public class Conference {

	private List<Track> tracks = new ArrayList<>();

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

}

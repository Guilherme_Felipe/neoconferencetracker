package br.com.conference.tracker.models;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Talk {

	private String title;
	private LocalTime time;
	private Integer length;

	public Talk(String title, Integer length) {
		this.title = title;
		this.length = length;
	}

	public Talk(String title, Integer length, LocalTime time) {
		this(title, length);
		this.time = time;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Override
	public String toString() {
		String formatedTime = time.format(DateTimeFormatter.ofPattern("hh:mma"));
		return String.format("%s %s %smin", formatedTime, this.title, this.length);
	}

}

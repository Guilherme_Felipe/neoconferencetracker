package br.com.conference.tracker.models;

import java.util.ArrayList;
import java.util.List;

public class Track {

	private Integer id;
	private List<Session> sessions = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}

	@Override
	public String toString() {
		return "Track " + id + ":";
	}

}

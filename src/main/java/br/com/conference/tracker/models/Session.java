package br.com.conference.tracker.models;

import java.util.ArrayList;
import java.util.List;

import br.com.conference.tracker.business.session.SessionPeriod;

public class Session {

	private SessionPeriod period;
	private List<Talk> talks = new ArrayList<>();

	public SessionPeriod getPeriod() {
		return period;
	}

	public void setPeriod(SessionPeriod period) {
		this.period = period;
	}

	public List<Talk> getTalks() {
		return talks;
	}

	public void setTalks(List<Talk> talks) {
		this.talks = talks;
	}

}

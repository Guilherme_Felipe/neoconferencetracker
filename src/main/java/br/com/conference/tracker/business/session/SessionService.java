package br.com.conference.tracker.business.session;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import br.com.conference.tracker.business.talk.TalkService;
import br.com.conference.tracker.models.Session;
import br.com.conference.tracker.models.Talk;
import br.com.conference.tracker.models.Track;

public class SessionService {

	private static final LocalTime MORNING_START_TIME = LocalTime.of(9, 0);
	private static final LocalTime MORNING_FINISH_TIME = LocalTime.NOON;

	private static final LocalTime AFTERNOON_START_TIME = LocalTime.of(13, 0);

	private static final LocalTime NETWORKING_MIN_START_TIME = LocalTime.of(16, 0);
	private static final LocalTime NETWORKING_MAX_START_TIME = LocalTime.of(16, 59);

	private TalkService talkService = new TalkService();

	public void createSessions(Track track) {
		Session morningSession = createMorningSession();
		track.getSessions().add(morningSession);

		Session afternoonSession = createAfternoonSession();
		track.getSessions().add(afternoonSession);
	}

	private Session createMorningSession() {
		Session session = new Session();
		session.setPeriod(SessionPeriod.MORNING);

		createSessionTalks(session, MORNING_START_TIME, MORNING_FINISH_TIME);

		createLunchTalk(session);

		return session;
	}

	private Session createAfternoonSession() {
		Session session = new Session();
		session.setPeriod(SessionPeriod.AFTERNOON);

		createSessionTalks(session, AFTERNOON_START_TIME, NETWORKING_MAX_START_TIME);

		createNetworkingTalk(session);

		return session;
	}

	private void createSessionTalks(Session session, LocalTime startTime, LocalTime lastEventTime) {
		List<Talk> toRemove = new ArrayList<>();

		List<Talk> allTasks = talkService.findAll();
		allTasks.forEach(talk -> {
			Talk lastTalk = talkService.findLastSesionTalk(session);
			if (lastTalk == null) {
				talk.setTime(startTime);
				session.getTalks().add(talk);
				toRemove.add(talk);
				return;
			}

			LocalTime lastTalkTime = lastTalk.getTime();
			LocalTime nextTime = lastTalkTime.plusMinutes(lastTalk.getLength());
			if (nextTime.plusMinutes(talk.getLength()).isBefore(lastEventTime)) {
				talk.setTime(lastTalkTime.plusMinutes(lastTalk.getLength()));
				session.getTalks().add(talk);
				toRemove.add(talk);
			}
		});

		talkService.deleteAll(toRemove);
	}

	private void createLunchTalk(Session session) {
		Talk networkingEvent = new Talk("Lunch", 60, MORNING_FINISH_TIME);
		session.getTalks().add(networkingEvent);
	}

	private void createNetworkingTalk(Session session) {
		Talk networkingEvent = new Talk("Networking Event", 60);

		Talk lastTalk = talkService.findLastSesionTalk(session);
		if (lastTalk == null) {
			networkingEvent.setTime(NETWORKING_MIN_START_TIME);
			session.getTalks().add(networkingEvent);
			return;
		}

		LocalTime lastTalkTime = lastTalk.getTime();
		Integer lastTalkLength = lastTalk.getLength();

		LocalTime nextTime = lastTalkTime.plusMinutes(lastTalkLength);
		if (nextTime.isBefore(NETWORKING_MIN_START_TIME)) {
			nextTime = NETWORKING_MIN_START_TIME;
		}

		networkingEvent.setTime(nextTime);
		session.getTalks().add(networkingEvent);
	}

}

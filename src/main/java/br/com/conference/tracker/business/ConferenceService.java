package br.com.conference.tracker.business;

import java.util.List;

import br.com.conference.tracker.business.talk.TalkService;
import br.com.conference.tracker.business.talk.TrackService;
import br.com.conference.tracker.models.Conference;
import br.com.conference.tracker.models.Track;

public class ConferenceService {

	private TalkService talkService = new TalkService();
	private TrackService trackService = new TrackService();

	public Conference createConference() {
		Conference conference = new Conference();
		List<Track> conferenceTracks = conference.getTracks();

		while (talkService.hasRemainingTalks()) {
			int trackId = conferenceTracks.size() + 1;
			conferenceTracks.add(trackService.createTrack(trackId));
		}

		conference.setTracks(conferenceTracks);

		return conference;
	}

}

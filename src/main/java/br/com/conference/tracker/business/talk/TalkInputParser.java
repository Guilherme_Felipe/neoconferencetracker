package br.com.conference.tracker.business.talk;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.com.conference.tracker.exceptions.InvalidTalkFormatException;
import br.com.conference.tracker.exceptions.TalkInputReadException;
import br.com.conference.tracker.models.Talk;

public class TalkInputParser {

	private static final String TALK_DATA_REGEX = "^([a-zA-Z]\\D+)\\s([0-9]+min|lightning)$";
	private static final Pattern TALK_DATA_PATTERN = Pattern.compile(TALK_DATA_REGEX, Pattern.CASE_INSENSITIVE);

	private static final String BOM_CHARACTER = "\uFEFF";

	public static void parseTalks(String path) {
		TalkDataset.getInstance().addAll(readInputData(path)
				.map(TalkInputParser::parseTalkLine)
				.collect(Collectors.toList()));
	}

	private static Stream<String> readInputData(String path) {
		try {
			return Files.lines(Paths.get(path), UTF_8);
		} catch (Exception e) {
			throw new TalkInputReadException(path);
		}
	}

	private static Talk parseTalkLine(String line) {
		Matcher matcher = TALK_DATA_PATTERN.matcher(line.replace(BOM_CHARACTER, ""));
		if (!matcher.matches() || matcher.groupCount() != 2) {
			throw new InvalidTalkFormatException(line);
		}

		String talkTitleGroup = matcher.group(1);
		String talkLengthGroup = matcher.group(2);

		String talkTitle = talkTitleGroup.trim();
		Integer talkLength = parseTalkLength(talkLengthGroup);

		return new Talk(talkTitle, talkLength);
	}

	private static Integer parseTalkLength(String talkLengthGroup) {
		if (talkLengthGroup.equalsIgnoreCase("lightning")) {
			return 5;
		}
		return Integer.parseInt(talkLengthGroup.replaceAll("\\D", ""));
	}

}

package br.com.conference.tracker.business.talk;

import java.util.ArrayList;
import java.util.List;

import br.com.conference.tracker.models.Talk;

public class TalkDataset {

	private static TalkDataset instance = new TalkDataset();
	private List<Talk> talks;

	private TalkDataset() {
		talks = new ArrayList<>();
	}

	public static TalkDataset getInstance() {
		return instance;
	}

	public void addAll(List<Talk> all) {
		talks.addAll(all);
	}

	public List<Talk> findAll() {
		return talks;
	}

	public int count() {
		return talks.size();
	}

	public void remove(Talk talk) {
		talks.remove(talk);
	}

	public void removeAll(List<Talk> toRemove) {
		talks.removeAll(toRemove);
	}

}

package br.com.conference.tracker.business.talk;

import java.util.List;

import br.com.conference.tracker.models.Session;
import br.com.conference.tracker.models.Talk;

public class TalkService {

	public List<Talk> findAll() {
		return TalkDataset.getInstance().findAll();
	}

	public Talk findLastSesionTalk(Session session) {
		return session.getTalks().stream()
				.max((talk, anotherTalk) -> talk.getTime().compareTo(anotherTalk.getTime()))
				.orElse(null);
	}

	public void deleteAll(List<Talk> talks) {
		TalkDataset.getInstance().removeAll(talks);
	}

	public boolean hasRemainingTalks() {
		return TalkDataset.getInstance().count() > 0;
	}

}

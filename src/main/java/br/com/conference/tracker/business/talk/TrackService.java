package br.com.conference.tracker.business.talk;

import br.com.conference.tracker.business.session.SessionService;
import br.com.conference.tracker.models.Track;

public class TrackService {

	private SessionService sessionService = new SessionService();

	public Track createTrack(Integer trackId) {
		Track track = new Track();
		track.setId(trackId);

		sessionService.createSessions(track);

		return track;
	}

}

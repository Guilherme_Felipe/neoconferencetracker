package br.com.conference.tracker.business.session;

public enum SessionPeriod {

	MORNING, AFTERNOON;

}

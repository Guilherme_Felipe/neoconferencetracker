package br.com.conference.tracker.exceptions;

public class InvalidTalkFormatException extends TalkException {

	private static final long serialVersionUID = 1L;

	public InvalidTalkFormatException(String line) {
		super(line);
	}

}

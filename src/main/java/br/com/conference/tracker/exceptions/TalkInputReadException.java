package br.com.conference.tracker.exceptions;

public class TalkInputReadException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String path;

	public TalkInputReadException(String path) {
		super();
		this.path = path;
	}

	@Override
	public String getMessage() {
		return String.format("Error reading input data from path '%s'", path);
	}

}

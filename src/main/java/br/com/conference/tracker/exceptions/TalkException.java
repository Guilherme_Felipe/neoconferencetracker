package br.com.conference.tracker.exceptions;

public class TalkException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	protected String line;

	public TalkException(String line) {
		super();
		this.line = line;
	}

	@Override
	public String getMessage() {
		return String.format("Error parsing line '%s'. Verify the line format.", line);
	}

}

package br.com.conference.tracker;

import br.com.conference.tracker.business.ConferenceService;
import br.com.conference.tracker.business.talk.TalkInputParser;
import br.com.conference.tracker.models.Conference;
import br.com.conference.tracker.utils.DisplayUtils;

public class ConferenceApp {

	public static void main(String[] args) {
		TalkInputParser.parseTalks(args[0]);

		Conference conference = new ConferenceService().createConference();

		DisplayUtils.displayConferenceTalks(conference);
	}

}

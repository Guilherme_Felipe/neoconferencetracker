package br.com.conference.tracker;

import java.time.LocalTime;
import java.util.List;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import br.com.conference.tracker.business.session.SessionPeriod;
import br.com.conference.tracker.business.session.SessionService;
import br.com.conference.tracker.business.talk.TalkDataset;
import br.com.conference.tracker.models.Session;
import br.com.conference.tracker.models.Talk;
import br.com.conference.tracker.models.Track;
import br.com.conference.tracker.utils.FakeTalkFacotry;

public class ConferenceAppTest {

	SessionService sessionService = new SessionService();

	@Test
	public void shouldCreateMorningAndAfternoonSessions() throws Exception {
		Track track = new Track();

		List<Talk> talkList = FakeTalkFacotry.createTalkList();
		TalkDataset.getInstance().addAll(talkList);

		sessionService.createSessions(track);

		List<Session> trackSessions = track.getSessions();

		Assert.assertThat(trackSessions.size(), Is.is(2));

		Session firstSession = trackSessions.get(0);
		Assert.assertThat(firstSession.getPeriod(), Is.is(SessionPeriod.MORNING));

		Session secondSession = trackSessions.get(1);
		Assert.assertThat(secondSession.getPeriod(), Is.is(SessionPeriod.AFTERNOON));
	}

	@Test
	public void shouldStartMorningSessionAtNineOClockAndLunchAtTwelveOClock() throws Exception {
		Track track = new Track();

		List<Talk> talkList = FakeTalkFacotry.createTalkList();
		TalkDataset.getInstance().addAll(talkList);

		sessionService.createSessions(track);

		List<Session> trackSessions = track.getSessions();

		Assert.assertThat(trackSessions.size(), Is.is(2));

		Session morningSession = trackSessions.get(0);
		Assert.assertThat(morningSession.getPeriod(), Is.is(SessionPeriod.MORNING));

		Talk firstTalk = morningSession.getTalks().stream()
				.min((talk1, talk2) -> talk1.getTime().compareTo(talk2.getTime())).get();
		Assert.assertThat(firstTalk.getTime(), Is.is(LocalTime.of(9, 0)));

		Talk lastTalk = morningSession.getTalks().stream()
				.max((talk1, talk2) -> talk1.getTime().compareTo(talk2.getTime())).get();
		Assert.assertThat(lastTalk.getTitle(), Is.is("Lunch"));
		Assert.assertThat(lastTalk.getTime(), Is.is(LocalTime.NOON));
	}

	@Test
	public void shouldStartAfternoonSessionAtOneOClockAndNetworkingAfterFourOClockAndBeforeFiveOClock() throws Exception {
		Track track = new Track();

		List<Talk> talkList = FakeTalkFacotry.createTalkList();
		TalkDataset.getInstance().addAll(talkList);

		sessionService.createSessions(track);

		List<Session> trackSessions = track.getSessions();

		Assert.assertThat(trackSessions.size(), Is.is(2));

		Session afternoonSession = trackSessions.get(1);
		Assert.assertThat(afternoonSession.getPeriod(), Is.is(SessionPeriod.AFTERNOON));

		Talk firstTalk = afternoonSession.getTalks().stream()
				.min((talk1, talk2) -> talk1.getTime().compareTo(talk2.getTime())).get();
		Assert.assertThat(firstTalk.getTime(), Is.is(LocalTime.of(13, 0)));

		Talk lastTalk = afternoonSession.getTalks().stream()
				.max((talk1, talk2) -> talk1.getTime().compareTo(talk2.getTime())).get();

		Assert.assertThat(lastTalk.getTime().isBefore(LocalTime.of(17, 0)), Is.is(true));
		Assert.assertThat(lastTalk.getTitle(), Is.is("Networking Event"));
	}

}

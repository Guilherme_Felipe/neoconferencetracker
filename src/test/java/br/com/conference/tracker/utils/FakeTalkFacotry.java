package br.com.conference.tracker.utils;

import java.util.Arrays;
import java.util.List;

import br.com.conference.tracker.models.Talk;

public class FakeTalkFacotry {

	public static Talk createTalk(String title, Integer length) {
		return new Talk(title, length);
	}

	public static List<Talk> createTalkList() {
		Talk talk1 = FakeTalkFacotry.createTalk("Talk 1", 30);
		Talk talk2 = FakeTalkFacotry.createTalk("Talk 2", 60);
		Talk talk3 = FakeTalkFacotry.createTalk("Talk 3", 30);
		Talk talk4 = FakeTalkFacotry.createTalk("Talk 4", 45);
		Talk talk5 = FakeTalkFacotry.createTalk("Talk 6", 5);

		Talk talk6 = FakeTalkFacotry.createTalk("Talk 7", 5);
		Talk talk7 = FakeTalkFacotry.createTalk("Talk 8", 30);
		Talk talk8 = FakeTalkFacotry.createTalk("Talk 9", 45);
		Talk talk9 = FakeTalkFacotry.createTalk("Talk 10", 45);

		return Arrays.asList(talk1, talk2, talk3, talk4, talk5, talk6, talk7, talk8, talk9);
	}

}
